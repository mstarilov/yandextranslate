package ru.test.gateway;

import com.google.gson.Gson;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import ru.test.entries.Translate;

@Slf4j
public class YandexTranslateGateway {
    private static final String URL = "https://translate.yandex.net/api/v1.5/tr.json/translate";
    private static final String TOKEN = "Ваш токен вида trnsl.1.1.20200115T053318Z.da9c75936a9f938....";

    @SneakyThrows
    public Translate getTranslate(String textForTranslate, String lang) {
        Gson gson = new Gson();
        HttpResponse<String> response = Unirest.get(URL)
                .queryString("key", TOKEN)
                .queryString("text", textForTranslate)
                .queryString("lang", lang)
                .asString();
        String strResponse = response.getBody();
        log.info("response: " + strResponse);
        return gson.fromJson(strResponse, Translate.class);
    }
}
