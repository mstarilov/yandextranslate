package ru.test.entries;

import com.google.gson.annotations.SerializedName;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class Translate {
    @SerializedName("text")
    public String [] text;
 }
