package ru.test;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import ru.test.entries.Translate;
import ru.test.gateway.YandexTranslateGateway;

public class YandexTranslateTest {
    private static final String TEXT = "Hello World!";
    private static final String LANG = "en-ru";
    private static final String [] TRANSLATED_TEXT = {"Всем Привет!"};

    @Test
    public void getTranslate(){
        YandexTranslateGateway yandexTranslateGateway = new YandexTranslateGateway();
        Translate translate = yandexTranslateGateway.getTranslate(TEXT, LANG);
        Assertions.assertArrayEquals(TRANSLATED_TEXT, translate.getText());
    }
}
